export default {
    axn_newPokemon({ commit }, pokemon) {
        commit('set_newPokemon', pokemon);
    },

    axn_nameFilter({ commit }, text) {
        commit('set_nameFilter', text);
    },

    axn_typeFilter({ commit }, type) {
        commit('set_typeFilter', type);
    }
}