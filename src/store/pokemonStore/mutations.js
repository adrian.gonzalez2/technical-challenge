export default {
    set_newPokemon(state, pokemon) {
        state.pokemons.push(pokemon);
    },

    set_nameFilter(state, value) {
        state.filters.name = value;
    },

    set_typeFilter(state, value) {
        state.filters.type = value;
    }
}