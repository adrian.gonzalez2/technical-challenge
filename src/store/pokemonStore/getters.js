export default {
    getOrderedPokemons(state) {
        return state.pokemons.sort((a, b) => a.order - b.order);
    },

    getFilteredPokemons(state, getters) {
        const pokemonNameFilter = state?.filters?.name;
        const pokemonTypeFilter = state?.filters?.type;

        const filteredPokemons = getters.getOrderedPokemons
            .filter((pokemon) => pokemon?.name?.includes(pokemonNameFilter))
            .filter((pokemon) => pokemon?.types
                .find((type) => type.type.name === pokemonTypeFilter || pokemonTypeFilter === 'Any')
            ) || [];

        return filteredPokemons; 
    },

    getPokemonTypes(state) {
        const types = state.pokemons
            .flatMap((pokemon) => pokemon?.types)
            .map(type => type?.type?.name)
            .sort() || [];

        return ['Any', ...new Set(types)];
    }
}