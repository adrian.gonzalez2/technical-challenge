import actions from './actions.js';
import mutations from './mutations.js';
import getters from './getters.js';

const defState = () => ({
    pokemons: [],
    filters: {
        name: '',
        type: 'Any'
    }
})

export default {
    state: defState(),
    mutations,
    actions,
    getters
}