import { computed } from 'vue';
import { useStore } from 'vuex';

export default function usePokemonFilters() {
    const store = useStore();
    const pokemonStore = store.state.pokemonStore;

    const pokemonTypes = computed(() => store.getters.getPokemonTypes.map(
        (type) => ({
            value: type,
            label: type
        })
    ));
    const pokemonNameFilter = computed(() => pokemonStore.filters.name);
    const pokemonTypeFilter = computed(() => pokemonStore.filters.type);
    const filteredPokemons = computed(() => store.getters.getFilteredPokemons);

    const setNameFilter = ({ target }) => {
        store.dispatch('axn_nameFilter', target.value);
    }

    const setTypeFilter = ({ target }) => {
        store.dispatch('axn_typeFilter', target.value);
    }

    return {
        filteredPokemons,
        pokemonTypes,
        pokemonNameFilter,
        pokemonTypeFilter,
        setNameFilter,
        setTypeFilter
    };
}