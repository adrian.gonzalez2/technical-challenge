import { useRoute } from 'vue-router';
import { useStore } from 'vuex';
import { computed } from 'vue';

export default function getPokemonInfo() {
    const store = useStore();
    const pokemonStore = store.state.pokemonStore;
    const route = useRoute();
    const pokemonId = route.params.id;

    const pokemonData = computed(() => pokemonStore.pokemons.find((pokemon) => pokemon.id == pokemonId));
    const pokemonTypes = computed(() => pokemonData.value.types
        .map(type => type.type.name)
        .join(', ')
    );

    return {
        pokemonData,
        pokemonTypes
    }
}