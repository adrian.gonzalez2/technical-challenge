import {screen, render} from '@testing-library/vue';
import {createStore} from 'vuex';
import {cloneDeep} from 'lodash';
import pokemonStore from '../../../src/store/pokemonStore'
import userEvent from '@testing-library/user-event'
import App from '../../../src/App.vue';
import router from '../../../src/router';

process.env.VUE_APP_POKEMON_API_URL = '';
const customRender = () => {
    const storeObj = () => ({
        modules: {
            pokemonStore: cloneDeep(pokemonStore)
        }
    })
    return render(App, {
        global: {
            plugins: [createStore(storeObj()), router]
        }
    })
};

global.fetch = jest.fn((id) =>
  Promise.resolve({
    json: () => Promise.resolve({
        id: 1, 
        name: `bulbasaur${id}`,
        order: 1,
        types: [
            {
                type: {
                    name: 'grass'
                }
            },
            {
                type: {
                    name: 'poison'
                }
            }
        ],
        sprites: {
            front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png'
        }
    }),
  })
);

describe('Pokemon view', () => {    
    it('Should render the pokemon info', async () => {
        customRender();

        const firstPokemonImage = await screen.findByAltText('bulbasaur/1');
        userEvent.click(firstPokemonImage);

        const pokemonName = await screen.findByText('bulbasaur/1');
        const pokemonId = await screen.findByText('1');
        const pokemonTypes = await screen.findByText('grass, poison');

        expect(pokemonName).toBeInTheDocument();
        expect(pokemonId).toBeInTheDocument();
        expect(pokemonTypes).toBeInTheDocument();
    })

    it('Should render the pokemon info', async () => {
        customRender();

        const firstPokemonImage = await screen.findByAltText('bulbasaur/1');
        userEvent.click(firstPokemonImage);

        const backButton = await screen.findByText('Back');
        userEvent.click(backButton);

        await screen.findByAltText('bulbasaur/2');
        
        expect(window.location.href).toEqual('http://localhost/#/');
    })

})