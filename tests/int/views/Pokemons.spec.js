import {screen, render} from '@testing-library/vue';
import {createStore} from 'vuex';
import {cloneDeep} from 'lodash';
import pokemonStore from '../../../src/store/pokemonStore'
import userEvent from '@testing-library/user-event'
import App from '../../../src/App.vue';
import router from '../../../src/router';

process.env.VUE_APP_POKEMON_API_URL = '';
const customRender = () => {
    const storeObj = () => ({
        modules: {
            pokemonStore: cloneDeep(pokemonStore)
        }
    })
    return render(App, {
        global: {
            plugins: [createStore(storeObj()), router]
        }
    })
};

global.fetch = jest.fn((id) =>
  Promise.resolve({
    json: () => Promise.resolve({
        id: 1, 
        name: `bulbasaur${id}`,
        order: 1,
        types: [
            {
                type: {
                    name: 'grass'
                }
            },
            {
                type: {
                    name: 'poison'
                }
            }
        ],
        sprites: {
            front_default: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/4.png'
        }
    }),
  })
);

describe('Pokemons view', () => {    
    it('Should render an input', async () => {
        customRender();

        const inputText = await screen.findByPlaceholderText('Write a pokemon name.');

        expect(inputText).toBeInTheDocument();
    })

    it('Should render an dropdown', async () => {
        customRender();

        const dropdown = await screen.findByLabelText('Pokemon Types');

        expect(dropdown).toBeInTheDocument();
    })

    it('Should load 151 images (pokemons)', async () => {
        customRender()

        const images = await screen.findAllByAltText(/bulbasaur/);
        expect(images.length).toEqual(151)
    })

    it('Should render 1 pokemon if user search by bulbasaur/1', async () => {
        customRender()

        const inputText = await screen.findByPlaceholderText('Write a pokemon name.');
        userEvent.type(inputText, 'bulbasaur/1');
        const images = await screen.findAllByAltText('bulbasaur/1');
        expect(images.length).toEqual(1)
    })

    it('Should render 151 pokemon if user search by bulbasaur', async () => {
        customRender();

        const inputText = await screen.findByPlaceholderText('Write a pokemon name.');
        userEvent.type(inputText, 'bulbasaur');
        const images = await screen.findAllByAltText(/bulbasaur/);
        expect(images.length).toEqual(151)
    })

    it('Should render 151 pokemon if user select poison type', async () => {
        customRender();

        const dropdown = await screen.findByLabelText('Pokemon Types');        
        userEvent.selectOptions(dropdown, ['poison']);
        const images = await screen.findAllByAltText(/bulbasaur/);
        expect(images.length).toEqual(151);
    })

    it('Shoul load pokemon route on pokemon click', async () => {
        customRender();
        const firstPokemonImage = await screen.findByAltText('bulbasaur/1');
        userEvent.click(firstPokemonImage);

        await screen.findAllByRole('list');

        expect(window.location.href).toContain('pokemon/1');
    })

})
